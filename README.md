Demo of comparing lookup in a `HashSet` with a `Vec` at various sizes. It creates a bunch of collections of each type, then looks in all of them for an element that doesn't exist and prints out timing measurements.

Usage:

```
cargo run
```
