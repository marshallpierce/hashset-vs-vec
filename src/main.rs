use std::collections::HashSet;
use std::time::{Instant, Duration};

fn main() {
    let num_collections = 1000;
    let max_collection_size = 10_000;

    let mut sets = Vec::new();
    let mut vecs = Vec::new();

    let mut collection_size = 1;
    while collection_size <= max_collection_size {
        sets.clear();
        vecs.clear();
        for _ in 0..num_collections {
            let mut set = HashSet::with_capacity(collection_size);
            let mut vec = Vec::with_capacity(collection_size);

            for i in 0..collection_size {
                set.insert(i);
                vec.push(i);
            }

            sets.push(set);
            vecs.push(vec);
        }

        // now, search through them all

        let mut found = 0;

        let set_start = Instant::now();
        for set in sets.iter() {
            // will not be found -- worst case for vec. However, we need to make sure the optimizer
            // doesn't remove the loop!
            if set.contains(&collection_size) {
                found += 1;
            }
        }
        let set_delta = Instant::now().duration_since(set_start);

        let vec_start = Instant::now();
        for vec in vecs.iter() {
            if vec.contains(&collection_size) {
                found += 1;
            }
        }
        let vec_delta = Instant::now().duration_since(vec_start);

        println!("Size {} done.\tHashSet time: {}\tVec time: {}\tFound: {}",
                 collection_size, as_ns(&set_delta), as_ns(&vec_delta), found);
        collection_size *= 2;
    }
}

/// We are measuring short durations, so we won't overflow u64 ns.
fn as_ns(dur: &Duration) -> u64 {
    ((dur.as_secs() as u64) * 1_000_000_000_u64).checked_add(dur.subsec_nanos() as u64).unwrap()
}

